use std::env;

const HEADER: &str = "Header";
const SOURCE: &str = "Source File";

fn get_file_type_name(name: &str) -> &'static str {
    if name.ends_with(".h") {
        return HEADER;
    }
    if name.ends_with(".cpp") {
        return SOURCE;
    }
    panic!("File with wrong Ending! {}", name);
}

fn get_filename(path: &str) -> &str {
    let last_slash = path.rfind("/");
    let name = match last_slash {
        None => path,
        Some(idx) => &path[(idx + 1)..],
    };
    name
}

fn print_one_include(filename: &str) {
    let name = get_filename(filename);

    let file_type = get_file_type_name(name);

    let sec_name = format!("{} {}", file_type, name);
    let sec_name = sec_name.replace("_", "\\_");
    println!("\\subsection{{{}}}", &sec_name);

    println!(
        "\\lstinputlisting[language=c++, style=customcpp, caption={}]{{{}}}",
        sec_name, filename
    );
}

fn main() {
    let mut args: Vec<String> = env::args().skip(1).collect();
    args.sort_unstable_by(|a, b| get_filename(b).cmp(get_filename(a)));
    for i in &args {
        print_one_include(i);
    }
}
